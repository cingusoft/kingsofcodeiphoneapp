var win = Titanium.UI.currentWindow;

var base64Image= "";

var MashapeHash = ""

var intervalRepeater = setInterval(function(){

  Ti.Media.takePicture();

},7000);

var scanner = Titanium.UI.createView({

  width:260,

  height:200,

  borderColor:'red',

  borderWidth:5,

  borderRadius:15

});

var overlay = Titanium.UI.createView();

overlay.add(scanner);

Titanium.Media.showCamera({



  success:function(event)

  {

    var image = event.media;

    var newWidth=200;

    var newHeight=(newWidth/image.width)*image.height;

    var imageView = Titanium.UI.createImageView({

            image:image,

            width:newWidth,

            height:newHeight

        });

        image = imageView.toImage();

        var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'camera_photo.png');

        f.write(image);

        var data_to_send = { 

            "file": f.read(), 

            "name": 'camera_photo.png' 

        };

        xhr = Titanium.Network.createHTTPClient();

        xhr.setRequestHeader("enctype", "multipart/form-data");

        xhr.setRequestHeader("Content-Type", "image/png");

        //is to solve the problem of Base64 image encoding, if we encode to base64 with Titanium the app crash!!!!!

        xhr.open("POST","http://ec2-46-137-142-71.eu-west-1.compute.amazonaws.com:8080/Mashape/hmac/upload");

        xhr.send(data_to_send);

        xhr.onload = function() {

          base64Image = JSON.parse(this.responseText);

          var loader = Ti.Network.createHTTPClient();

      var publicKey = "YOUR_PUBLIC_KEY";

      var privateKey = "YOUR_PRIVATE_KEY";

      //with this call will be create the mashape hash string

      var url = "http://ec2-46-137-142-71.eu-west-1.compute.amazonaws.com:8080/Mashape/hmac/"+publicKey+"/"+privateKey;

      loader.open('GET',url);

      loader.onload = function(){

        var keyHeader = JSON.parse(this.responseText);

        MashapeHash = keyHeader.encod;

        var loaderQrcode = Ti.Network.createHTTPClient();

        loaderQrcode.open('POST','J02s0rHNxCJvmUzeaAMBzvWGW.proxy.mashape.com');

        loaderQrcode.setRequestHeader("X-Mashape-Authorization",MashapeHash);

        loaderQrcode.onload = function(){

          //var qrcode = JSON.parse(this.responseText);

          var qrcode = JSON.parse(this.responseText);

          if(qrcode.result == undefined){

            //Ti.Media.takePicture();

          }else{

            clearInterval(intervalRepeater);

            var speechLoader = Ti.Network.createHTTPClient();

            //this is a private call to text to speech, this api is in ALPHA, if you are interested to use it, please send me an email cingusoft@gmail.com

            speechLoader.open('GET',"http://send-me-an-email-for-mashape-proxy/"+escape(qrcode.result));

            speechLoader.setRequestHeader("X-Mashape-Authorization",MashapeHash);

            speechLoader.onload = function(){

              

              var musicFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,'voice.mp3');

              var datMusic = JSON.parse(this.responseText);

              musicFile.write(Ti.Utils.base64decode(datMusic.data));

              var sound = Titanium.Media.createSound({

                          sound: musicFile.read(),

                          preload: true

                      });

                sound.play();

            };

            speechLoader.send();  

          }

          

        };

        loaderQrcode.onerror = function(){

          var a = Titanium.UI.createAlertDialog({title:'Gen Alert'});

          var error = JSON.stringify(this.responseText);

          a.setMessage(error);

          a.show();

        };

        loaderQrcode.send({barcode:base64Image.encod});

      };

      loader.send();

        };

        xhr.onerror = function(){

          var a = Titanium.UI.createAlertDialog({title:'Camera'});

          a.setMessage("error");

          a.show();

        }

  },

  cancel:function()

  {

  },

  error:function(error)

  {

    var a = Titanium.UI.createAlertDialog({title:'Camera'});

    if (error.code == Titanium.Media.NO_CAMERA)

    {

      a.setMessage('Please run this test on device');

    }

    else

    {

      a.setMessage('Unexpected error: ' +  error.code);

    }

    a.show();

  },

  overlay:overlay,

  showControls:false,  // don't show system controls

  mediaTypes:Ti.Media.MEDIA_TYPE_PHOTO,

  autohide:false, // tell the system not to auto-hide and we'll do it ourself

});