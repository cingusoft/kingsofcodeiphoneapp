// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');

var tabGroup = Titanium.UI.createTabGroup();

var win1 = Titanium.UI.createWindow({  
    title:'Kings of code',
    backgroundColor:'#fff',
    url: 'views.js'
});

var mainTab = Titanium.UI.createTab ({  
    title: "Amazing App", // Title of the tab: "Twitter"  
    icon: "eye.png", // Icon for tab, Included in the source files  
    window: win1 // We will create the window "mainWin"  
});

tabGroup.addTab(mainTab);
tabGroup.open();
